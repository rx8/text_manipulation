import re

def readLines (filename):
	f = open(filename,"r")
	f2 = open("test_new","w")
	# line = f.readline()
	# print ("ReadLine goes: %s",line)
	# lines = f.readlines(10)
	# print ("Readlines goes: %s",lines)

	for line in f:
		capitalize_women(line,f2)
		contains_blue_devil(line)
		find_non_terminal_said(line)

	f.close()

def capitalize_women(line,f2):
	newLine = line.replace("women","WOMEN")
	f2.writelines(newLine)

def contains_blue_devil(line):
	if "Blue Devil" in line:
		print ("Found Blue Devil")

def find_non_terminal_said(line):
	prog = re.compile('men')
	result = prog.match(line)
	print(result)

if __name__ == "__main__":
	#readLines("example_text.txt")
	readLines("test.txt")


#2: Readline import the formatted text, readlines import original text
